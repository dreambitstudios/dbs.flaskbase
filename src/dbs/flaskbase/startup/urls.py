import logging


logger = logging.getLogger(__name__)


def setup(app):
    logger.info('Starting urls setup.')
    modules = app.config.get('MODULES')

    for module in modules:
        logger.info('Importing module %s urls' % module)
        part = __import__(module, globals(), locals(), ['urls', 'module'], -1)

        for url, view in part.urls.patterns.iteritems():
            part.module.add_url_rule(url, view_func=view)
