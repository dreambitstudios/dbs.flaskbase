import logging


logger = logging.getLogger(__name__)


def setup(app):
    modules = app.config.get('MODULES')

    for module in modules:
        logger.info('Registering %s module.' % module)
        part = __import__(module, globals(), locals(), ['module'], -1)
        app.register_blueprint(part.module)
