from dbs.flaskbase.utils.autoconvert import autoconvert


class Settings(object):
    pass


def config2object(config):
    settings = Settings()

    for section in config.sections():
        for item in config.items(section):
            setattr(settings,
                    '%s_%s' % (section.upper(), item[0].upper()),
                    autoconvert(item[1]))

    return settings
