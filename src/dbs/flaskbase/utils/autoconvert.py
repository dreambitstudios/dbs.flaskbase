def boolify(item):
    return {'True': True, 'False': False}[item]


def autoconvert(item):
    for method in (boolify, int, float):
        try:
            return method(item)
        except:
            pass
    return item
