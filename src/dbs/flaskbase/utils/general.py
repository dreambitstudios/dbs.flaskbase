def chunks(l, n):
    """ Returns list that is splitted to n number of lists
    http://stackoverflow.com/a/2136090/558194
    """
    return [l[i::n] for i in xrange(n)]
