import logging


logger = logging.getLogger(__name__)


def startup(app):
    for startup in app.config.get('STARTUP'):
        logger.info('Starting %s' % startup)
        part = __import__(startup, globals(), locals(), ['setup'], -1)
        part.setup(app)

    return app
